import _ from 'lodash'
import React from 'react'
import DocumentTitle from 'react-document-title'
import { connect } from 'react-redux'

import Header from '../components/header.jsx'

class App extends React.Component {

    getSuggestions() {
        const { data } = this.props
        const suggestions = []
        if (data.tags) {
            suggestions.push({
                title: 'tag:',
                description: 'Filter resources based on tags',
                query: 'tag:'
            })
        }

        if (data.securityDefinitions && _.find(data.securityDefinitions, { type: 'oauth2' })) {
            suggestions.push({
                title: 'scope:',
                description: 'Filter resources based on the required OAuth2 scope',
                query: 'scope:'
            })
        }
        return suggestions
    }

    render() {
        const { children, data, location } = this.props
        return (
            <DocumentTitle title={data.info.title}>
                <section id="content" role="main">
                    <Header
                      title={data.info.title}
                      suggestions={this.getSuggestions()}
                      location={location}
                    />
                    {children}
                </section>
            </DocumentTitle>)
    }
}

App.propTypes = {
    children: React.PropTypes.object,
    data: React.PropTypes.object.isRequired,
    location: React.PropTypes.object
}

export default connect(state => state, {})(App)
