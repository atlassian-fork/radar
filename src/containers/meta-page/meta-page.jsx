import './meta-page.less'

import React from 'react'
import { connect } from 'react-redux'

import Page from '../../components/aui/page/page.jsx'
import MetaContent from '../../components/meta-content/meta-content.jsx'

const MetaPage = ({ data, params }) => {
    for (const page of data['x-radar-pages']) {
        if (page.key === params.name) {
            return (<Page>
                <MetaContent page={page} />
            </Page>)
        }
    }
    return null
}

MetaPage.propTypes = {
    data: React.PropTypes.object,
    params: React.PropTypes.shape({
        name: React.PropTypes.string
    })
}

export default connect(s => s, {})(MetaPage)
