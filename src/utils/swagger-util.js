/**
 * Ensures the base path does not end in a slash.
 */
export function cleanBasePath(value) {
    const val = value || ''
    return val.slice(-1) === '/' ? val.substring(0, val.length - 1) : val
}

export function addDefinitionTitles(data) {
    if (data.definitions) {
        Object.keys(data.definitions).forEach(key => {
            if (!data.definitions[key].title) {
                data.definitions[key].title = key
            }
        })
    }

    return data
}
