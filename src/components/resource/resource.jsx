import './resource.less'

import _ from 'lodash'
import React from 'react'

import CodeList from '../resource-components/code-list.jsx'
import Description from '../resource-components/description/description.jsx'
import ExternalDocs from '../external-docs/external-docs.jsx'
import Path from '../resource-components/path.jsx'
import Parameters from '../resource-components/parameters/parameters.jsx'
import Responses from '../resource-components/responses/responses.jsx'
import RequestBody from '../resource-components/request-body/request-body.jsx'
import Security from '../resource-components/security/security.jsx'
import Tabs from '../aui/tabs/tabs.jsx'
import { getLozengeClass, getSupportedMethods } from '../../utils/method-util'
import { cleanBasePath } from '../../utils/swagger-util'
import { hash } from '../../utils/window-util'

export default class Resource extends React.Component {

    static getBody(resource) {
        return _.find(resource.parameters, param => param.in === 'body')
    }

    static methodSection(method, methodResource, path, securityDefinitions) {
        return (<div className="method">
            <Description descriptionId={`${method}-summary`} html={methodResource.summary} />
            <Description
              descriptionId={`${method}-description`}
              html={methodResource.description}
            />
            {ExternalDocs.fromOptionalObject(methodResource.externalDocs)}
            <Security definitions={securityDefinitions} security={methodResource.security} />
            <CodeList title="Produces" values={methodResource.produces} />
            <Parameters resource={methodResource} smallHeader />
            <RequestBody body={Resource.getBody(methodResource)} />
            <Responses resource={methodResource} />
        </div>)
    }

    static methodLozenge(method) {
        return (<span
          key={`${method}-lozenge`}
          className={`aui-lozenge aui-lozenge-subtle ${getLozengeClass(method)}`}
        >
            {method}
        </span>)
    }

    render() {
        const { basePath, path, resource, securityDefinitions } = this.props

        const tabValues = getSupportedMethods(resource).map(method => {
            const methodResource = resource[method]
            const content = Resource
                .methodSection(method, methodResource, path, securityDefinitions)
            const label = method.toUpperCase()
            return { content, label, id: method }
        })
        const methodLozenges = getSupportedMethods(resource)
            .map(method => Resource.methodLozenge(method))
        const resourceName = path.split('/').slice(-1)[0]
        return (<div className="resource">
            <h1>{resourceName}</h1>
            <Path value={cleanBasePath(basePath) + path} />
            <Parameters resource={resource} />
            <h2>Methods {methodLozenges}</h2>
            <Tabs
              values={tabValues}
              layout={Tabs.options().layout.HORIZONTAL}
              selectedTab={hash()}
            />
        </div>)
    }
}

Resource.propTypes = {
    basePath: React.PropTypes.string,
    path: React.PropTypes.string.isRequired,
    resource: React.PropTypes.object.isRequired,
    securityDefinitions: React.PropTypes.object
}
