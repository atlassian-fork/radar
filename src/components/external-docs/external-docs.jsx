import React from 'react'
import Markdown from 'react-markdown'

export default class ExternalDocs extends React.Component {

    static fromOptionalObject(props) {
        return props ? <ExternalDocs {...props} /> : null
    }

    render() {
        const { description, url } = this.props
        return (<div className="external-docs">
            <a href={url} target="_blank">
                {description ? <Markdown source={description} skipHtml /> :
                'More information...'}
            </a>
        </div>)
    }
}

ExternalDocs.propTypes = {
    description: React.PropTypes.string,
    url: React.PropTypes.string.isRequired
}
