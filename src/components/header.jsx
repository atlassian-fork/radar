import React from 'react'

import Search from './search/search.jsx'

const Header = ({ title, suggestions, location }) => (
    <header className="aui-page-header">
        <div className="aui-page-header-inner">
            <div className="aui-page-header-main">
                <h2>{title}</h2>
            </div>
            <div className="aui-page-header-actions search-container">
                <Search suggestions={suggestions} location={location} />
            </div>
        </div>
    </header>
)

Header.propTypes = {
    title: React.PropTypes.string,
    suggestions: React.PropTypes.arrayOf(React.PropTypes.object),
    location: React.PropTypes.object
}

export default Header
