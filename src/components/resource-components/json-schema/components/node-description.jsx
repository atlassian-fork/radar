import React from 'react'
import Markdown from 'react-markdown'

const NodeDescription = ({ value }) => {
    if (!value) {
        return null
    }
    return <span className="node-description"><Markdown source={value} skipHtml /></span>
}

NodeDescription.propTypes = {
    value: React.PropTypes.string
}

export default NodeDescription
