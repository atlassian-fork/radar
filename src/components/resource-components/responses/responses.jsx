import './responses.less'

import React from 'react'
import Markdown from 'react-markdown'

import Example from '../example.jsx'
import SchemaExample from '../schema-example/schema-example.jsx'

const Responses = ({ resource }) => {
    if (resource.responses) {
        return (<div className="resource-section">
            <h4>Responses</h4>
            <table className="aui resource-responses">
                <thead>
                    <tr>
                        <th key="status" className="status">Status Code</th>
                        <th key="desc" className="description">Description</th>
                        <th key="example" className="example">Response</th>
                    </tr>
                </thead>
                <tbody>
                {Object.keys(resource.responses).map(responseCode => {
                    const schema = resource.responses[responseCode].schema
                    const examples = !!resource.produces ? resource.produces.map(contentType => {
                        if (resource.responses[responseCode].examples &&
                            resource.responses[responseCode].examples[contentType]) {
                            const src = resource.responses[responseCode].examples[contentType]
                            return <Example key={`${contentType}-example`} value={src} />
                        }
                        return null
                    }) : null
                    return (
                        <tr key={responseCode}>
                            <td key="code">{responseCode}</td>
                            <td key="desc">
                                <Markdown
                                  source={resource.responses[responseCode].description}
                                  skipHtml
                                />
                            </td>
                            <td key="example">
                                <SchemaExample
                                  exampleKey={responseCode}
                                  schema={schema}
                                  example={schema && schema.example ? schema.example : null}
                                />
                                {examples}
                            </td>
                        </tr>)
                })}
                </tbody>
            </table>
        </div>)
    }
    return null
}

Responses.propTypes = {
    resource: React.PropTypes.object
}

export default Responses
