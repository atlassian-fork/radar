import './request-body.less'

import React from 'react'

import Example from '../example.jsx'
import JsonSchema from '../json-schema/json-schema.jsx'

const RequestBody = ({ body }) => {
    if (body && body.schema) {
        let result
        if (body.schema.example) {
            result = <Example value={body.schema.example} />
        } else {
            result = <JsonSchema data={body.schema} hideReadOnly />
        }

        return (<div className="resource-request resource-section">
            <h4>Request example</h4>
            {result}
        </div>)
    }
    return null
}

RequestBody.propTypes = {
    body: React.PropTypes.shape({
        schema: React.PropTypes.object
    })
}

export default RequestBody
