import './description.less'

import React from 'react'
import Markdown from 'react-markdown'

export default class Description extends React.Component {

    render() {
        const { title, descriptionId, html } = this.props
        if (!html || html.length === 0) {
            return null
        }

        const titleEl = title ? <h3 id={title}>{title}</h3> : null
        return (
            <div key={descriptionId} className="resource-description">
                {titleEl}
                <Markdown source={html} skipHtml />
            </div>)
    }
}

Description.propTypes = {
    title: React.PropTypes.string,
    descriptionId: React.PropTypes.string,
    html: React.PropTypes.string
}
