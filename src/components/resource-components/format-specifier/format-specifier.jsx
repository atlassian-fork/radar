import './format-specifier.less'

import React from 'react'
import { connect } from 'react-redux'

// Formats that are defined by the Swagger specification.
const KNOWN_FORMATS = {
    date: {
        displayName: 'date',
        description: 'an ISO-8601 date in the "full-date" format',
        href: 'http://xml2rfc.ietf.org/public/rfc/html/rfc3339.html#anchor14'
    },
    'date-time': {
        displayName: 'date-time',
        description: 'an ISO-8601 date in the "date-time" format',
        href: 'http://xml2rfc.ietf.org/public/rfc/html/rfc3339.html#anchor14'
    },
    password: {
        displayName: 'password',
        description: 'property contains a password, avoid displaying it in clear text'
    }
}

// Applications can provide their own format descriptions by storing an object
// with the following key in the Swagger document.  Its schema is the same as
// KNOWN_FORMATS.
const CUSTOM_FORMATS_EXTENSION_PROPERTY = 'x-radar-format-specifiers'

// Return the properties for a format if it is in KNOWN_FORMATS or customFormats;
// otherwise we'll just display the format name as-is.
function describeFormat(format, customFormats) {
    if (KNOWN_FORMATS[format]) {
        return KNOWN_FORMATS[format]
    }
    if (customFormats && customFormats[format]) {
        return customFormats[format]
    }
    return { displayName: format }
}

const FormatSpecifier = ({ customFormats, format }) => {
    const f = describeFormat(format, customFormats)
    return (<span className="format-specifier" title={f.description}>
        {f.href ? <a href={f.href}>{f.displayName}</a> : f.displayName}
    </span>)
}

FormatSpecifier.propTypes = {
    customFormats: React.PropTypes.shape({
        [React.PropTypes.string]: React.PropTypes.shape({
            displayName: React.PropTypes.string.isRequired,
            description: React.PropTypes.string.isRequired,
            href: React.PropTypes.string.isRequired
        })
    }),
    format: React.PropTypes.string.isRequired
}

const WrappedFormatSpecifier = connect(s => ({
    customFormats: s.data[CUSTOM_FORMATS_EXTENSION_PROPERTY]
}), {})(FormatSpecifier)

export default WrappedFormatSpecifier

export function optionalFormatSpecifier(format) {
    return format ? <WrappedFormatSpecifier format={format} /> : null
}
