import React from 'react'
import Highlight from 'react-highlight'

export default class Example extends React.Component {

    static formatJson(value) {
        let val
        try {
            val = JSON.parse(value)
        } catch (e) {
            val = value
        }
        try {
            return JSON.stringify(val, null, 4)
        } catch (e) {
            // Ignore
        }
        return String(value)
    }

    render() {
        const { value } = this.props
        if (value) {
            // Assume everything is JSON, because it pretty much is.
            return (<Highlight className="json">
                    {Example.formatJson(value)}
            </Highlight>)
        }
        return null
    }
}

Example.propTypes = {
    value: React.PropTypes.any
}
