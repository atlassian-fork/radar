/* eslint-disable no-console */
const compression = require('compression')
const express = require('express')
const fs = require('fs')
const httpProxy = require('http-proxy')
const Radar = require('../index')

const PORT = 8080

/**
 * RADAR can be configured with a basePath and a contextPath that provides
 * some flexibility when sharing a domain with other services. RADAR can be
 * hosted down in a subdirectory, as opposed to claiming the domain's root
 * directly.
 *
 * The context path is essentially a basePath, but it is only used for routing.
 * basePath is used only as a prefix for static assets.
 */
const basePath = process.env.RADAR_BASE_PATH || ''
const contextPath = process.env.RADAR_CONTEXT_PATH || basePath

const app = express()
const proxy = httpProxy.createProxyServer()
app.use(compression())
app.use('/images', express.static(Radar.STATIC_PATHS.images))
app.all('/js/*', (req, res) => {
    proxy.web(req, res, {
        // Proxy any JS requests to the webpack dev server
        target: 'http://localhost:9090/'
    })
})

/**
 * A test endpoint.
 */
app.get('/docs/:product/:version*', (req, res, next) => {
    const requestObject = {
        product: req.params.product,
        version: req.params.version,
        filename: 'swagger.json'
    }
    const swaggerContent = fs.readFileSync(
        `test-data/${requestObject.product}/${requestObject.version}/swagger.json`,
        { encoding: 'utf-8' })
    const context = contextPath || (`/docs/${requestObject.product}/${requestObject.version}`)
    Radar.render(swaggerContent, context, {
        basePath,
        devMode: '<div style="background-color: #205081; color: white">' +
                 '<center>~Dev Mode~</center></div>'
    }).then((html) => {
        res.status(200).send(html)
    }).catch((err) => {
        if (err) {
            next(err)
            return
        }
    })
})

/**
 * Any errors logged to the console will be picked up by the
 */
app.use((err, req, res, next) => {
    if (err) {
        console.error(err)
    }
    next()
})

/**
 * Catch any proxy errors.
 */
proxy.on('error', (e) => {
    console.error('Could not connect to proxy, please try again...', e)
})

/**
 * Fall back to a 404 as a catch-all error page.
 * Order is important here. Express requires this to be the last
 * defined `app.use`, as handlers are called in the order they are
 * registered in.
 */
app.use((req, res) => {
    if (req.accepts('html')) {
        Radar.notFound().then((html) => {
            res.status(404).send(html)
        }).catch((err) => {
            if (err) {
                console.error(err)
                res.status(500).send()
            }
        })
        return
    }

    if (req.accepts('json')) {
        res.status(404).send({ error: 'Not found' })
        return
    }

    res.type('txt').send('Not found')
})

app.listen(PORT)
